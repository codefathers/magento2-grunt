#!/usr/bin/env bash

# stop on errors
set -e

# stop on errors
INSTALLFLAG="${WORKDIR}/.node-installed"

if [ -z "${WORKDIR}" ]; then
    WORKDIR="/data"
fi

cd "${WORKDIR}"

init() {
    if [ ! -f "package.json" ]; then
        echo "not a grunt root: '${WORKDIR}/package.json' is missing"
        exit 1
    fi
    npm install
    npm update
    touch "${INSTALLFLAG}"
    chmod a+w "${INSTALLFLAG}"
}

CMD="${1}"

case $CMD in
   init)
        init
        exit 0
        ;;
   enter)
        /bin/bash
        exit 0
        ;;
   env)
        env
        exit 0
        ;;
   dir)
        ls -la
        exit 0
        ;;
   *)
        if [ ! -f "${INSTALLFLAG}" ]; then
            init
        fi
        grunt ${@}
        ;;
esac

