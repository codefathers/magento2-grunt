# Codefathers Docker Image for running GRUNT tasks in a Magento 2 Docker environment

Example width some "env" vars:

~~~
    docker run -it --rm --user www-data:www-data \
        -v "${ROOT_PATH}":"${DOCKER_ROOT}" \
        -v "${ROOT_PATH}/../var/media":"${MAGE_ROOT}/pub/media" \
        -v "${ROOT_PATH}/../var/magento":"${MAGE_ROOT}/var" \
        -v "${ROOT_PATH}/config/php/dev/php.ini":"/usr/local/etc/php/php.ini" \
        -w "${MAGE_ROOT}" \
        -p "35729:35729" \
        -e WORKDIR="${MAGE_ROOT}" \
        -e MAGE_ROOT="${MAGE_ROOT}" \
        -e MAGE_MODE="${MAGE_MODE}" \
        --net=${COMPOSE_PROJECT_NAME}_back \
        --sig-proxy=true \
        --pid=host \
        --link ${COMPOSE_PROJECT_NAME}_mysql_1:mysql \
        --link ${COMPOSE_PROJECT_NAME}_cache_1:rediscache \
        --link ${COMPOSE_PROJECT_NAME}_sessions_1:redissession \
        codefathers/magento2-grunt watch -v
~~~

