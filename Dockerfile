FROM php:7.1-fpm

# copy startup script
COPY entrypoint.sh /usr/local/bin/entrypoint.sh

RUN set -x \
    && chmod 755 /usr/local/bin/entrypoint.sh \
    && DEBIAN_FRONTEND=noninteractive \
    && apt-get update \
    && apt-get install -qy --no-install-recommends \
        ruby \
        ruby-dev \
        gem \
        locales \
        build-essential \
        automake \
        autoconf \
        gcc \
        libtool \
        binutils \
        libxml2-dev \
        libcurl4-openssl-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng12-dev \
        libbz2-dev \
        libmcrypt-dev \
        libmm-dev \
        libxslt1-dev \
        mysql-client \
        libicu-dev \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) \
            gd \
            mbstring \
            mysqli \
            pdo_mysql \
            mcrypt \
            iconv \
            soap \
            zip \
            xsl \
            intl \
    && gem update --system \
    && gem install compass bourbon \
    && curl -sL https://deb.nodesource.com/setup_8.x | /bin/bash - \
    && apt-get install -qy --no-install-recommends nodejs \
    && npm install -g gulp grunt grunt-cli \
    && apt-get update \
    && apt-get purge -y --auto-remove \
    && apt-get autoremove -y ruby-dev \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && mkdir -p /usr/local/etc/php /data /var/www \
    && chmod 777 /var/www \
    && chown www-data:www-data /var/www \
    && sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
    && echo 'LANG="en_US.UTF-8"'>/etc/default/locale \
    && dpkg-reconfigure --frontend=noninteractive locales \
    && update-locale LANG=en_US.UTF-8


COPY php.ini /usr/local/etc/php/php.ini
COPY php-cli.ini /usr/local/etc/php/php-cli.ini

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# Define working directory.
WORKDIR /data

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

# start here
# CMD ["entrypoint.sh"]
